# Install singularity

https://singularity-tutorial.github.io/01-installation/


# build container

```
sudo singularity build ubuntu_wine32.sif ubuntu_wine32.def
sudo singularity build ubuntu_wine64.sif ubuntu_wine64.def
```


# Interactive run commands in container

```
singularity shell -B /mnt/ -B /groups -B /home ubuntu_wine32.sif 
```

The `-B` (bind) argument specifies locations which will be available inside the container

You are now in the running container and can start `cmd` in wine by

```
wine cmd
```

or run hawc2 in wine

```
wine <path_to_hawc2>/hawc2mb.exe htc/test.htc
```

Note, `<path_to_hawc2>` must be inside one of the one of the bound locations

